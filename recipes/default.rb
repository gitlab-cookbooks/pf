service 'pf' do
  action [:enable, :start]
end

service 'pflog' do
  action [:enable, :start]
end

pf_conf = '/etc/pf.conf'

firewall_rules = nil
firewall_rules = node['firewall']['rules'] if node['firewall']
firewall_rules ||= []

template pf_conf do
  notifies :run, 'execute[load pf.conf]', :immediately
  variables(rules: firewall_rules)
end

execute 'load pf.conf' do
  command "pfctl -f #{pf_conf}"
  action :nothing
end

execute 'pfctl -e' do
  not_if "pfctl -sa | grep '^Status: Enabled'"
end
