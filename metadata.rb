name             'pf'
maintainer       'GitLab B.V.'
maintainer_email 'jacob@gitlab.com'
license          'All rights reserved'
description      'Installs/Configures pf'
long_description 'Installs/Configures pf'
version          '0.1.0'

supports 'freebsd'
