# pf-cookbook

Basic cookbook for managing the PF firewall on FreeBSD.

## Supported Platforms

FreeBSD 9.3

## Attributes

The only required attribute within a rule is 'port'. If 'protocol' is
empty, assume TCP + UDP.

```json
{
  "firewal": {
    "rules": [
    {
      "rule name": {
        "port": "1235",
        "source": "1.2.3.4/5",
        "destination": "2.3.4.5/6",
        "protocol": "udp"
      }
    }
    ]
  }
}

```
## Usage

### pf::default

Enable pf and pf_log, write pf.conf and enable the firewall.

## License and Authors

Author:: YOUR_NAME (<YOUR_EMAIL>)
